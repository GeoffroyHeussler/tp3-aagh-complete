package protocol;

import java.util.ArrayList;
import java.util.List;

public class ProtocolMultipleEntity {
    private List<ProtocolSingleEntity> data;

    public ProtocolMultipleEntity (){
        data = new ArrayList<>();
    }

    public ProtocolMultipleEntity(List<ProtocolSingleEntity> prevData){
        data = prevData;
    }

    public void addEntity(ProtocolSingleEntity entity){
        data.add(entity);
    }

    public void removeEntity(ProtocolSingleEntity entity){
        data.remove(entity);
    }

    public List<ProtocolSingleEntity> getData(){
        return data;
    }
}
