package protocol;

import enums.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProtocolSingleEntity {
    private Map<DataTypeI, String> data;

    public ProtocolSingleEntity(){
        data = new HashMap<>();
    }

    public ProtocolSingleEntity(Map<DataTypeI, String> data){
        this.data = data;
    }

    public Map<DataTypeI, String> getPackage(){
        return new HashMap<>(data);
    }

    public void emptyPackage(){
        data.clear();
    }

    public void removeField(DataTypeI type){
        data.remove(type);
    }

    //region ========== PREFACE Data ==========
    public void addSection(SectionType type){
        assert type != null : "type shouldn't be null !";
        data.put(DataType.SECTION, type.toString());
    }

    public SectionType getSection() throws IllegalArgumentException {
        SectionType retVal = null;
        try{
            retVal = SectionType.valueOf(data.get(DataType.SECTION));
        } catch (IllegalArgumentException e){
            throw new IllegalArgumentException("Absence de données de type SECTION");
        }
        return retVal;
    }

    public void addAction(ActionType type){
        assert type != null : "type shouldn't be null !";
        data.put(DataType.ACTION, type.toString());
    }

    public ActionType getAction() throws IllegalArgumentException{
        ActionType retVal = null;
        try{
            retVal = ActionType.valueOf(data.get(DataType.ACTION));
        }catch(IllegalArgumentException e){
            throw new IllegalArgumentException("Absence de données de type ACTION");
        }
        return retVal;
    }
    //endregion

    //region ========== CLIENT Data ==========
    public void addClientString(ClientInfoType type, String value){
        assert type != null : "type shouldn't be null !";
        assert value != null : "value shouldn't be null !";
        assert !value.trim().isEmpty() : "value shouldn't be empty !";

        data.put(type, value);
    }

    public String getClientString(ClientInfoType type){
        assert type != null : "type shouldn't be null !";
        return data.get(type);
    }

    public void addClientField(ClientInfoType field){
        assert field != null : "field shouldn't be null !";
        addField(field);
    }

    public Set<ClientInfoType> getClientFields(){
        Set<ClientInfoType> retVal = new HashSet<>();

        if(data.containsKey(DataType.FIELD)){
            List<String> listFields = Arrays.asList(data.get(DataType.FIELD).split(","));
            for(String str : listFields){
                retVal.add(ClientInfoType.valueOf(str));
            }
        }

        return retVal;
    }
    //endregion

    //region ========== RESERVATION Data ==========
    public void addResInt(ReservationInfoType type, int value){
        assert type != null : "type shouldn't be null !";
        data.put(type, ""+value);
    }

    public Integer getResInt(ReservationInfoType type){
        assert type != null : "type shouldn't be null !";
        return data.get(type) != null ? Integer.parseInt(data.get(type)) : null;
    }

    public void addResDate(ReservationInfoType type, Date date){
        assert type != null : "type shouldn't be null !";
        assert date != null : "Date shouldn't be null";

        data.put(type, date.toString());
    }

    public Date getResDates(ReservationInfoType type){
        assert type != null : "type shouldn't be null !";
        DateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        Date retVal = null;
        try {
            retVal = data.get(type) != null ? format.parse(data.get(type)) : retVal;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    public void addResString(ReservationInfoType type, String value){
        assert type != null : "type shouldn't be null !";
        assert value != null : "value shouldn't be null !";

        data.put(type, value);
    }

    public String getResString(ReservationInfoType type){
        assert type != null : "type shouldn't be null !";
        return data.get(type);
    }

    public void addResField(ReservationInfoType field){
        assert field != null : "field shouldn't be null !";
        addField(field);
    }

    public Set<ReservationInfoType> getResFields(){
        Set<ReservationInfoType> retVal = new HashSet<>();

        if(data.containsKey(DataType.FIELD)){
            List<String> listFields = Arrays.asList(data.get(DataType.FIELD).split(","));
            for(String str : listFields){
                retVal.add(ReservationInfoType.valueOf(str));
            }
        }

        return retVal;
    }
    //endregion

    //region UTILITY
    private void addField(DataTypeI field){
        if(data.containsKey(DataType.FIELD)){
            Set<String> fields = new HashSet<>(Arrays.asList(data.get(DataType.FIELD).split(",")));
            fields.add(field.toString());
            String fieldsStr = "";
            for(String str : fields){
                fieldsStr += str+",";
            }
            fieldsStr = fieldsStr.substring(0, fieldsStr.length()-1);
            data.put(DataType.FIELD, fieldsStr);
        }else{
            data.put(DataType.FIELD, field.toString());
        }
    }
    //endregion
}
