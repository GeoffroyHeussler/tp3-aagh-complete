package model;

import logger.LoggerUtility;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/***
 * Class {@link Database}
 */
public class Database {

    private Connection dbConn = null;
    private PreparedStatement dbPrepareStat = null;
    private static DatabaseMetaData md = null;
    //Log error
    private LoggerUtility loggerUtility;

    /**
     * Class {@link Database} constructor.
     */
    public Database() {
        loggerUtility = new LoggerUtility(this);
        makeJDBCConnection();
    }

    //region METHODES ================  DATABASE ==============================================
    private void makeJDBCConnection() {

        //Enregistrement du driver JDBC
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            loggerUtility.getLOGGER().info("Congrats - Seems your MySQL JDBC Driver Registered!");
        } catch (ClassNotFoundException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error message " + e.getMessage() +" time: "+ System.currentTimeMillis());
            return;
        }

        // DriverManager: service pour gérer les connections avec le driver JDBC
        try {
            dbConn = DriverManager.getConnection("jdbc:mysql://localhost/tp3_hotel?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=America/New_York", "root", "root");
            if (dbConn != null) {
                loggerUtility.getLOGGER().info("Connection Successful! Enjoy. Now it's time to push data");
            } else {
                loggerUtility.getLOGGER().info("Failed to make connection!");
            }
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
            return;
        }

        //metadata de la BD pour checker l'existence des columns d'une table par ex
        try {
            md = dbConn.getMetaData();
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

    }

    public void closeDB() {
        try {
            if (dbPrepareStat != null)
                dbPrepareStat.close();
            loggerUtility.closeLogger();
            //clear le fichier log
            //loggerUtility.clearLogFileContent();
            dbConn.close();
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() + " " + dbConn.toString() + "DB is closed");
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

    }

    public Connection getDbConn() {
        return dbConn;
    }

    //endregion

    //region METHODES ================  CLIENT ==============================================

    /***
     * Méthode pour ajouter un client avec une requête INSERT
     * et d'insérer les valeurs associées à chaque champs avec le bon type
     * dans une list de Map avec un nom de colonne et la valeur en question
     * @param usrName
     * @param email
     * @param firstname
     * @param name
     * @return true si l'action s'est bien passée
     */
    public boolean addClientToDB(String usrName, String email, String firstname, String name) {

        int ok = 0;
        try {
            String insertQueryStatement = "INSERT  INTO  clients_cli  VALUES  (?,?,?,?)";

            dbPrepareStat = dbConn.prepareStatement(insertQueryStatement);
            dbPrepareStat.setString(1, usrName);
            dbPrepareStat.setString(2, email);
            dbPrepareStat.setString(3, firstname);
            dbPrepareStat.setString(4, name);

            // execute insert SQL statement
            dbPrepareStat.executeUpdate();
            ok = dbPrepareStat.getUpdateCount();
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

        return ok > 0;

    }

    /***
     * Méthode qui permet de récupérer chaque enregistrement en bouclant dans unn ResultSet retourné suite à l'execution d'une requête
     * et d'insérer les valeurs associées à chaque champs avec le bon type
     * dans une list de Map avec un nom de colonne et la valeur en question
     * @param usr_name
     * @param fields
     * @return soit null si l'execution de la requete s'est mal passé, soit le retour de la méthode resultSetToMap (voir méthode suivante)
     */
    public List<Map<String, Object>> getClientFromDB(String usr_name, List<String> fields) {
        //TODO check for null
        ResultSet rs = null;
        try {
            // MySQL Select Query Tutorial

            String getQueryStatement = queryBuilderSelectClient(usr_name, fields);
            dbPrepareStat = dbConn.prepareStatement(getQueryStatement);

            // Execute the Query, and get a java ResultSet
            rs = dbPrepareStat.executeQuery();
            log(ActionType.SELECT.toString() + "Client from db sucess " + dbPrepareStat.toString());

        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

        return rs != null ? resultSetToMap(rs) : null;
    }

    /***
     * Méthode qui permet de récupérer chaque enregistrement en bouclant dans unn ResultSet retourné suite à l'execution d'une requête
     * et d'insérer les valeurs associées à chaque champs avec le bon type
     * dans une list de Map avec un nom de colonne et la valeur en question
     * @param rs
     * @return une liste contenant une map qui a pour clef le nom de la colonne et la valeur associé à cette colonne pour un enregistrement
     */
    private List<Map<String, Object>> resultSetToMap(ResultSet rs) {
        List<Map<String, Object>> retVal = new ArrayList<>();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();

            while (rs.next()) {
                Map<String, Object> map = new HashMap<>();
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    String name = rsmd.getColumnName(i);
                    String typeName = rsmd.getColumnTypeName(i);

                    //switch columName compare avec enum if(colum type = date = > rs.getdate par exemple)
                    switch (typeName.toUpperCase()) {
                        case "DATE":
                            map.put(name, rs.getDate(name));
                            break;
                        case "VARCHAR":
                            map.put(name, rs.getString(name));
                            break;
                        default:
                            if (typeName.toUpperCase().trim().matches("INT")) {
                                map.put(name, rs.getInt(name));
                            }
                            break;
                    }
                    //map.put(name, rs.getObject(name));
                }
                retVal.add(map);
            }
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

        return retVal;
    }

    /***
     * Méthode pour faire un DELETE d'un client grâce à nom d'usager
     * @param usrname
     * @return true si l'action s'est bien passée
     */
    public boolean deleteClientFromDB(String usrname) {
        int ok = 0;
        String getQueryStatement = "DELETE FROM clients_cli WHERE cli_usr_name= ?";

        try {
            dbPrepareStat = dbConn.prepareStatement(getQueryStatement);
            dbPrepareStat.setString(1, usrname);
            dbPrepareStat.executeUpdate();
            ok = dbPrepareStat.getUpdateCount();
            log(ActionType.DELETE.toString() + " Client success:" + dbPrepareStat.toString());
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

        return ok > 0;
    }

    /***
     * Méthode pour faire un UPDATE d'un client grâce à nom d'usager
     * @param usr_name
     * @param data
     * @return true si l'action s'est bien passée
     */
    public boolean updateClientFromDB(String usr_name, Map<String, String> data) {

        String getQueryStatement = "";
        int ok = 0;

        getQueryStatement = queryBuilderUpdateClient(usr_name, data);

        try {
            // prepare
            dbPrepareStat = dbConn.prepareStatement(getQueryStatement);
            //execution
            dbPrepareStat.executeUpdate();
            //retour > 0 si ok
            ok = dbPrepareStat.getUpdateCount();
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }
        return ok > 0;
    }
    //endregion

    //region METHODES ================  RESERVATION ==============================================
    /***
     * Méthode pour faire une réservation de chambre avec tous les paramètres nécessaires sauf l'id (gérer automatiquement par la BD)
     * @param dateArr
     * @param dateDep
     * @param comment
     * @param usrName
     * @param numCh
     * @return true si l'action s'est bien passée
     */
    public boolean addReservationToDB(Date dateArr, Date dateDep, String comment, String usrName, int numCh) {
        //TODO pas d'ID (autoincrement), res_date reçoit la sysdate (juste date)
        java.sql.Date sDate = new java.sql.Date(new Date().getTime());
        //16/11/2016
        int ok = 0;

        try {
            String insertQueryStatement = "INSERT  INTO  reservations_res (res_cha_num, res_cli_usr_name, res_comment, res_date, res_date_arr, res_date_dep) VALUES  (?,?,?,?,?,?)";

            dbPrepareStat = dbConn.prepareStatement(insertQueryStatement);
            dbPrepareStat.setInt(1, numCh);
            dbPrepareStat.setString(2, usrName);
            dbPrepareStat.setString(3, comment);
            dbPrepareStat.setDate(4, sDate);
            dbPrepareStat.setDate(5, (java.sql.Date) dateArr);
            dbPrepareStat.setDate(6, (java.sql.Date) dateDep);

            // execute insert SQL statement
            dbPrepareStat.executeUpdate();
            ok = dbPrepareStat.getUpdateCount();
            log("Reservation " + ActionType.INSERT.toString() + "successfully " + dbPrepareStat.toString());
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

        return ok > 0;

    }

    /***
     * Permet d'effacer un enregistrement dans la table reservation
     * @param idRes
     * return true si l'action s'est bien passée
     */
    public List<Map<String, Object>> getReservationFromDB(int idRes, List<String> fields) {
        //TODO check for null **** int can not be null *** or -1 for idRes (= *)
        ResultSet rs = null;
        String getQueryStatement = null;
        try {
            getQueryStatement = queryBuilderSelectReservation(idRes, fields);
            dbPrepareStat = dbConn.prepareStatement(getQueryStatement);

            // Execute the Query, and get a java ResultSet
            rs = dbPrepareStat.executeQuery();

        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

        return rs != null ? resultSetToMap(rs) : null;
    }

    /***
     * Permet d'effacer un enregistrement dans la table reservation
     * @param idRes
     * return true si l'action s'est bien passée
     */
    public boolean deleteReservationFromDB(int idRes) {
        int ok = 0;
        String getQueryStatement = "DELETE FROM clients_cli WHERE cli_usr_name= ?";

        try {
            dbPrepareStat = dbConn.prepareStatement(getQueryStatement);
            dbPrepareStat.setInt(1, idRes);
            dbPrepareStat.executeUpdate();
            ok = dbPrepareStat.getUpdateCount();
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }
        return ok > 0;
    }

    /***
     * permet de construire une requete UPDATE pour la table reservations en mappant les champs passer en parametre au enum {@link ReservationDateType}
     * La fonction permet de modifier les date d,arrivée de départ et de réservation ainsi que le commentaire
     * @param idRes
     * @param dateData
     * @param comment
     * return query
     */
    public boolean updateReservationFromDB(int idRes, Map<String, Date> dateData, String comment) {
        //TODO check for comment == null (ça veut dire qu'on n'update pas le comment)

        String getQueryStatement = "UPDATE reservations_res SET ";
        int ok = 0;
        try {
            Date dateArr = null;
            Date dateDep = null;

            for (Map.Entry<String, Date> entry : dateData.entrySet()) {
                String columnToUpdate = entry.getKey();
                Date value = entry.getValue();

                if (columnToUpdate.trim().toUpperCase().equals(ReservationInfoType.DATE_ARR.toString())) {
                    dateArr = new Date(value.getTime());
                    columnToUpdate = "res_date_dep";
                } else if (columnToUpdate.trim().toUpperCase().equals(ReservationInfoType.DATE_DEP.toString())) {
                    dateDep = new Date(value.getTime());
                    columnToUpdate = "res_date_arr";
                } else if (columnToUpdate.trim().toUpperCase().equals(ReservationInfoType.DATE_RES.toString())) {
                    columnToUpdate = "res_date";
                }
                getQueryStatement += columnToUpdate + "='" + value + "',";
            }
            if ((dateArr != null && dateDep != null) && dateArr.compareTo(dateDep) > 0) {
                log("Problem UPDATE reservations : departure date must be before arrival date OR missing DATE");
                return false;
            }
            if (getQueryStatement.endsWith(",") && comment == null) {
                getQueryStatement = getQueryStatement.substring(0, getQueryStatement.length() - 1) + " ";
            } else {
                getQueryStatement += "res_comment=" + comment;
            }
            getQueryStatement += " WHERE res_id=" + idRes;

            // prepare
            dbPrepareStat = dbConn.prepareStatement(getQueryStatement);
            //execution
            dbPrepareStat.executeUpdate();
            //retour > 0 si ok
            ok = dbPrepareStat.getUpdateCount();
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }
        return ok > 0;
    }
    //endregion


    //region METHODES ================  UTILITY ==============================================
    /***
     * permet de construire une requete SELECT pour la table client en mappant les champs passer en parametre au enum {@link ClientInfoType}
     * @param usr_name
     * @param fields
     * return query
     */
    public String queryBuilderSelectClient(String usr_name, List<String> fields) {
        String ret = "SELECT ";

        if (fields != null) {
            for (String field : fields) {
                if (ClientInfoType.ID.toString().equals(field)) {
                    ret += "cli_usr_name,";
                } else if (ClientInfoType.PRENOM.toString().equals(field)) {
                    ret += "cli_prenom,";
                } else if (ClientInfoType.NOM.toString().equals(field)) {
                    ret += "cli_nom,";
                } else if (ClientInfoType.EMAIL.toString().equals(field)) {
                    ret += "cli_email,";
                }
                ret += field + ",";
            }
            if (ret.endsWith(",")) {
                ret = ret.substring(0, ret.length() - 1) + " ";
            }
        } else {
            ret += " * ";
        }

        if (usr_name == null) {
            ret += " FROM clients_cli";
        } else {
            ret += "FROM clients_cli WHERE usr_name=" + usr_name;
        }

        return ret;
    }

    /***
     * permet de construire une requete UPDATE pour la table client en mappant les champs passer en parametre au enum {@link ClientInfoType}
     * @param usr_name
     * @param data
     * return query
     */
    public String queryBuilderUpdateClient(String usr_name, Map<String, String> data) {
        String getQueryStatement = "UPDATE clients_cli SET ";

        for (Map.Entry<String, String> entry : data.entrySet()) {
            String columnToUpdate = entry.getKey();
            String value = entry.getValue();

            if (ClientInfoType.ID.toString().equals(columnToUpdate)) {
                columnToUpdate = "cli_usr_name";
            } else if (ClientInfoType.PRENOM.toString().equals(columnToUpdate)) {
                columnToUpdate = "cli_prenom";
            } else if (ClientInfoType.NOM.toString().equals(columnToUpdate)) {
                columnToUpdate = "cli_nom";
            } else if (ClientInfoType.EMAIL.toString().equals(columnToUpdate)) {
                columnToUpdate = "cli_email";
            }
            getQueryStatement += columnToUpdate + "='" + value + "',";
        }
        if (getQueryStatement.endsWith(",")) {
            getQueryStatement = getQueryStatement.substring(0, getQueryStatement.length() - 1) + " ";
        }

        getQueryStatement += " WHERE cli_usr_name='" + usr_name + "'";

        return getQueryStatement;
    }

    /***
     * permet de construire une requete SELECT pour la table reservations en mappant les champs passer en parametre au enum {@link ReservationInfoType}
     * @param res_id
     * @param fields
     * return query
     */
    public String queryBuilderSelectReservation(int res_id, List<String> fields) {
        String ret = "SELECT ";

        if (fields != null) {
            for (String field :
                    fields) {
                if (ReservationInfoType.ID.toString().equals(field)) {
                    field = "res_id,";
                } else if (ReservationInfoType.DATE_RES.toString().equals(field)) {
                    field = "res_date,";
                } else if (ReservationInfoType.DATE_ARR.toString().equals(field)) {
                    field = "res_date_arr,";
                } else if (ReservationInfoType.COMMENT.toString().equals(field)) {
                    field = "res_comment,";
                } else if (ReservationInfoType.USR_NAME.toString().equals(field)) {
                    field = "res_cli_usr_name,";
                } else if (ReservationInfoType.NUM_CH.toString().equals(field)) {
                    field = "res_cha_num,";
                }
                ret += field + ",";
            }
            if (ret.endsWith(",")) {
                ret = ret.substring(0, ret.length() - 1) + " ";
            }
        } else {
            ret += " * ";
        }

        if (res_id == -1) {
            ret += " FROM reservations_res";
        } else {
            ret += "FROM reservations_res WHERE res_id=" + res_id;
        }

        return ret;
    }

    public boolean checkID_exists(String table, String field, String id) {

        Boolean exists = false;
        String getQueryStatement = "SELECT * FROM " + table + " WHERE " + field + " = ?";

        try {
            dbPrepareStat = dbConn.prepareStatement(getQueryStatement);
            dbPrepareStat.setString(1, id);
            ResultSet rs = dbPrepareStat.executeQuery();
            //FIXME checker les values dans le resultset car ici c faux
            if (rs.next()) {
                exists = true;
            }
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }
        return exists;
    }

    public boolean checkTable_exists(String table) {

        boolean exists = false;

        try {

            ResultSet rs = md.getTables(null, null, table, null);
            if (rs.next()) {
                exists = true;
                log("table exists ");
            }

        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }

        return exists;

    }

    private boolean checkColumn_exists(String table, String columnName) {

        boolean exists = false;
        try {
            ResultSet rs = md.getColumns(null, null, table, columnName);

            if (rs.next()) {
                exists = true;
                log("column exists ");
            }
        } catch (SQLException e) {
            loggerUtility.getLOGGER().info("Class: "+ this.getClass().toString() +" - Error code "+ e.getErrorCode() + e.getMessage() +" time: "+ System.currentTimeMillis());
        }
        return exists;
    }

    /***
     * permet l'affichage simple d'un messsage en console
     * @param message
     */
    private static void log(String message) {
        System.out.println(message);

    }

    //endregion

    public static void main(String[] argv) {

        Database db = new Database();

//        db.addClientToDB("bouliche_95","bibi@yahoo.fr", "bibi", "baba");
//        db.getClientFromDB("bibi@yahoo.fr");

//        db.updateInfo("clients_cli", "cli_nom", "bisou", "cli_usr_name", "bouliche_95");
        List<String> fields = Arrays.asList("cli_usr_name", "cli_email", "cli_prenom");
//        db.getClientFromDB(null, fields);

        HashMap<String, String> data = new HashMap<>();
        data.put(ClientInfoType.EMAIL.toString(), "test_new_email@yahoo.fr");
        data.put(ClientInfoType.NOM.toString(), "bonjourno");
        data.put(ClientInfoType.PRENOM.toString(), "maurice");

        db.updateClientFromDB("bouliche_95", data);

//        db.deleteClientFromDB("bouliche_95");

        DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
        java.sql.Date sqlDateArr = null;
        java.sql.Date sqlDateDep = null;
        try {
            sqlDateArr = new java.sql.Date(df.parse("02-04-2015").getTime());
            sqlDateDep = new java.sql.Date(df.parse("10-04-2015").getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //db.addReservationToDB(sqlDateArr, sqlDateDep, "un commentaire", "bouliche_95", 1);

//        List <Map<String, Object>> test = db.getClientFromDB(null, null);
//
//        System.out.println("La lste retourne pour CLIENTS : " + test.toString());
//
        fields = Arrays.asList("res_cha_num", "res_date_dep");
//        List<Map<String, Object>> test = db.getReservationFromDB(-1, fields);
//
//        System.out.println("La lste retourne pour RESERVATIONS : " + test.toString());

//        Map<String, Date> testUpdateResa = new HashMap<>();
//        java.sql.Date sqlDateRes = null;
//        Protocol prot = new Protocol();
//
//        try {
//            sqlDateArr = new java.sql.Date(df.parse("02-04-2018").getTime());
//            sqlDateDep = new java.sql.Date(df.parse("10-04-2017").getTime());
//            sqlDateRes = new java.sql.Date(df.parse("10-04-2016").getTime());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        prot.addResDate(ReservationDateType.DATE_DEP, sqlDateArr);
//        prot.addResDate(ReservationDateType.DATE_ARR, sqlDateArr);
//        prot.addResDate(ReservationDateType.DATE_RES, sqlDateRes);
//
//        testUpdateResa.put(ReservationDateType.DATE_DEP.toString(), sqlDateDep);
//        testUpdateResa.put("arrivee", sqlDateArr);
//
//        System.out.println("La resa update reponse : " + db.updateReservationFromDB(1, prot.getResDates(), null));

        db.closeDB();


    }


}
