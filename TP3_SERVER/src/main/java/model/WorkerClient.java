package model;

import enums.*;
import protocol.ProtocolSingleEntity;

import javax.net.ssl.SSLSocket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;

public class WorkerClient implements Runnable{
    protected Socket soc;
    private Database db;

    public WorkerClient(SSLSocket clientSocket, Database db){
        soc = clientSocket;
        this.db = db;
    }

    @Override
    public void run() {
        boolean keepGoing = true;
        try {
            ObjectOutputStream out = new ObjectOutputStream(soc.getOutputStream());
            ObjectInputStream is = new ObjectInputStream(soc.getInputStream());
            while (keepGoing) {
                ProtocolSingleEntity op = new ProtocolSingleEntity((Map<DataTypeI, String>) is.readObject());
                ActionType action = op.getAction();
                op.removeField(DataType.ACTION);
                switch (action){
                    case INSERT:
                        if(op.getSection() == SectionType.CLIENT){
                            insertClient(op);
                        }else{
                            insertReservation(op);
                        }
                        break;
                    case UPDATE:
                        if(op.getSection() == SectionType.CLIENT){
                            updateClient(op);
                        }else {
                            updateReservation(op);
                        }
                        break;
                    case SELECT:
                        //for (int i = 1; i <= rsmd.getColumnCount(); i++) String name = rsmd.getColumnName(i);
                        if(op.getSection() == SectionType.CLIENT){
                            db.getClientFromDB(op.getClientId(), op.getClientFields());
                        }else {
                            db.getReservationFromDB(op.getResID(), op.getResFields());
                        }
                        break;
                    case DELETE:
                        if(op.getSection() == SectionType.CLIENT){
                            deleteClient(op);
                        }else {
                            deleteReservation(op);
                        }
                        break;
                    case QUITTER:
                        keepGoing = false;

                        //soc.close(); FIXME nécessaire ?
                        is.close();
                        out.close();
                        break;
                }
            }
        } catch (SocketException e){
            System.out.println("Le client a crashé");
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            //TODO Message du client mal formé
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    //TODO à utiliser après avoir récup l'info des select
    private Protocol prepSelectReturnPackage(List<Map<String, String>> infos){
        Protocol retVal = new Protocol();
        List<Protocol> listProt = new ArrayList<>();
        Set<String> setInfoType = new HashSet<>();

        for (Map<String, String> map : infos) {
            Protocol proto = new Protocol();
            for (Map.Entry<String, String> entry : map.entrySet()){
                String name = entry.getKey();
                switch(name){
                    case "cli_usr_name":
                        proto.addClientID(entry.getValue());
                        setInfoType.add(ClientInfoType.ID.toString());
                        break;
                    case "cli_email":
                        proto.addClientInfo(ClientInfoType.EMAIL, entry.getValue());
                        setInfoType.add(ClientInfoType.EMAIL.toString());
                        break;
                    case "cli_nom":
                        proto.addClientInfo(ClientInfoType.NOM, entry.getValue());
                        setInfoType.add(ClientInfoType.NOM.toString());
                        break;
                    case "cli_prenom":
                        proto.addClientInfo(ClientInfoType.PRENOM, entry.getValue());
                        setInfoType.add(ClientInfoType.PRENOM.toString());
                        break;
                }
            }
            listProt.add(proto);
        }

        retVal.addListInfoClient(listProt);
        retVal.addListInfoType(setInfoType);

        return retVal;
    }

    //TODO les errors sont à logger et dans le run utiliser le boolean pour savoir si l'on doit envoyer au client un message disant : Une erreur est survenue
    //TODO passer les retVal à true si tout s'est bien passé !
    private boolean updateClient(ProtocolSingleEntity data){
        data.removeField(DataType.SECTION);
        boolean retVal = false;
        String usrname = data.getClientString(ClientInfoType.ID);
        data.removeField(ClientInfoType.ID);
        Map<String, String> infos = data.getClientInfos();

        if(usrname != null && infos != null){
            db.updateClientFromDB(usrname, infos);
        }else{
            //TODO error
        }

        return retVal;
    }

    private boolean insertClient(ProtocolSingleEntity data){
        data.removeField(DataType.SECTION);
        boolean retVal = false;
        String usrname = data.getClientId();
        String email, nom, prenom;
        Map<String, String> infos = data.getClientInfos();

        if(usrname != null &&
                infos != null &&
                (email = infos.get(ClientInfoType.EMAIL.toString())) != null &&
                (nom = infos.get(ClientInfoType.NOM.toString())) != null &&
                (prenom = infos.get(ClientInfoType.PRENOM.toString())) != null){
            db.addClientToDB(usrname, email, prenom, nom);
        }else{
            //TODO error
        }

        return retVal;
    }

    private boolean deleteClient(ProtocolSingleEntity data){
        data.removeField(DataType.SECTION);
        boolean retVal = false;
        String usrname = data.getClientId();

        if(usrname != null){
            db.deleteClientFromDB(usrname);
        }else{
            //TODO error
        }

        return retVal;
    }

    private boolean updateReservation(ProtocolSingleEntity data){
        data.removeField(DataType.SECTION);
        boolean retVal = false;
        Integer id = data.getResID();
        Map<String, Date> infos = data.getResDates() == null ? new HashMap<>() : data.getResDates();
        String comment = data.getResComment();

        if(id != null && (!infos.isEmpty() || comment != null)){
            db.updateReservationFromDB(id, infos, comment );
        }else{
            //TODO error
        }

        return retVal;
    }

    private boolean insertReservation(ProtocolSingleEntity data){
        data.removeField(DataType.SECTION);
        boolean retVal = false;
        Map<String, Date> dates = data.getResDates();
        String comment = data.getResComment();
        String client = data.getResClient();
        Integer numCh = data.getResNumCh();
        Date dateArr, dateDep;

        if(dates != null &&
                (dateArr = dates.get(ReservationDateType.DATE_ARR.toString())) != null &&
                (dateDep = dates.get(ReservationDateType.DATE_DEP.toString())) != null &&
                comment != null &&
                client != null &&
                numCh != null){
            db.addReservationToDB(dateArr, dateDep, comment, client, numCh);
        }else{
            //TODO error
        }

        return retVal;
    }

    private boolean deleteReservation(ProtocolSingleEntity data){
        data.removeField(DataType.SECTION);
        boolean retVal = false;
        Integer id = data.getResID();

        if(id != null){
            db.deleteReservationFromDB(id);
        }else{
            //TODO error
        }

        return retVal;
    }

    private Map<String, String> getClientInfos(ProtocolSingleEntity data){
        data.removeField(DataType.SECTION);
        Map<String, String> retVal = new HashMap<>();

        if(data.getClientString(ClientInfoType.NOM) != null)
            retVal.put(null);
    }
}
