package logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.*;

/**
 * Class permettant ce créer un logger et
 * de l'utiliser pour toutes les classes du projets
 *
 */
public class LoggerUtility {

    private Handler fileHandler = null;
    private Formatter simpleFormatter = null;
    private Logger LOGGER;

    /**
     * Constructor de la class. Il faut passer le THIS pour créer un logger
     * @param objClass afin de créer un objet Logger
     */
    public LoggerUtility(Object objClass) {
        LOGGER = Logger.getLogger(objClass.getClass().getName());
        addHandler();
    }

    /**
     * Cette méthode va créer un fichier de LOG commun à toutes les classes qui utilise cette classe
     * Voir racine du projet
     */
    private void addHandler() {
        fileHandler = null;
        simpleFormatter = null;
        try {

            // FileHandler A la racine du projet car pose des problemes
            fileHandler = new FileHandler("./logError.formatter.log", true);

            // Creating SimpleFormatter
            simpleFormatter = new SimpleFormatter();

            // Assigning handler to logger
            LOGGER.addHandler(fileHandler);

            // Setting formatter to the handler
            fileHandler.setFormatter(simpleFormatter);

            // Setting Level to ALL
            fileHandler.setLevel(Level.ALL);
            LOGGER.setLevel(Level.ALL);

            // For a Simple format message
            LOGGER.finest("Finnest message: Logger with SIMPLE FORMATTER");
        } catch (IOException exception) {
            LOGGER.log(Level.SEVERE, "Error occur in FileHandler.", exception);
        }

    }

    /**
     * Methode qui doit être appeler avant de fermer l'application
     * void
     */
    public void closeLogger() {
        for (Handler h : LOGGER.getHandlers()) {
            h.close();
        }
    }

    /**
     * Getter permettant de récupérer le logger courant
     * @return logger
     */
    public Logger getLOGGER() {
        return LOGGER;
    }

    /**
     * Efface le contenu des logs sans effacer le fichier .log
     * void
     */
    public void clearLogFileContent() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("./logError.formatter.log");
        } catch (FileNotFoundException e) {
            LOGGER.info("class: "+ this.getClass().getName() +" error :" + e.getMessage());
        }
        writer.print("");
        writer.close();
    }


}
